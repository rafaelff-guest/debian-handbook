msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2022-07-30 18:23+0200\nPO-Revision-Date: 2022-10-19 18:06+0000\nLast-Translator: d <dmanye@gmail.com>\nLanguage-Team: Catalan <https://hosted.weblate.org/projects/debian-handbook/70_conclusion/ca/>\nLanguage: ca-ES\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n != 1;\nX-Generator: Weblate 4.14.2-dev\n"

msgid "Future"
msgstr "Futur"

msgid "Improvements"
msgstr "Millores"

msgid "Opinions"
msgstr "Opinions"

msgid "Conclusion: Debian's Future"
msgstr "Conclusió: el futur de Debian"

msgid "The story of Falcot Corp ends with this last chapter; but Debian lives on, and the future will certainly bring many interesting surprises."
msgstr "La història de Falcot Corp acaba amb aquest darrer capítol, però Debian continua i el futur de ben cert que portarà moltes sorpreses interessants."

msgid "Upcoming Developments"
msgstr "Propers desenvolupaments"

msgid "Now that Debian version 11 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Bookworm</emphasis>…"
msgstr "Ara que Debian versió 11 s'ha publicat, els desenvolupadors ja tornen a estar ocupats treballant per a la següent versió, anomenada <emphasis role=\"distribution\">Bookworm</emphasis>…"

msgid "There is no official list of all planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends and discussion topics can already be noted, and we can try to guess what might happen (or not). Some of the expected changes are documented in the Debian 11 release notes: <ulink type=\"block\" url=\"https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#deprecated-components\" />"
msgstr "No hi ha cap llista oficial amb tots canvis planificats, i Debian mai fa promeses relatives als objectius tècnics de les següents versions. No obstant això, algunes tendències de desenvolupament i temes de discussió es poden observar, i podem intentar endevinar què pot passar (o no). Alguns dels canvis esperats estan documentats a les notes de publicació de Debian 11: <ulink type=\"block\" url=\"https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#deprecated-components\" />"

msgid "Beyond usual deprecation of some software components, it is worth pointing out that Debian is in the process of switching to what is known as a <emphasis>merged-usr filesystem</emphasis>: in this scheme <literal>/bin</literal>, <literal>/sbin</literal> and <literal>/lib</literal> are symlinks pointing to the corresponding directories in <literal>/usr</literal>. This improves compatibility between all Unix systems, makes us closer of having all Debian-supplied files in a single top-level directory that can be easily protected, snapshotted or shared. You can learn more about the benefits here: <ulink type=\"block\" url=\"https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/\" />"
msgstr "Més enllà de l'obsolescència habitual d'alguns components de programari, val la pena assenyalar que Debian està en procés de canviar al que es coneix com a sistema de fitxers «<emphasis>merged-usr-filesystem</emphasis>» (o “sistema de fitxers «usr» unificat”): en aquest esquema <literal>/bin</literal>, <literal>/sbin</literal> i <literal>/lib</literal> són enllaços simbòlics que apunten als directoris corresponents de <literal>/usr</literal>. Això millora la compatibilitat entre tots els sistemes Unix, ens fa més a prop de tenir tots els fitxers subministrats per Debian en un únic directori de nivell superior que fàcilment es pot protegir, fer-ne “instantànies” (o «snapshots») o compartir. Podeu aprendre més sobre els beneficis aquí: <ulink type=\"block\" url=\"https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/\" />"

msgid "This important change is not without creating issues: <command>dpkg</command> will have to learn about those aliased directories, but the <command>dpkg</command> maintainer doesn't like the technical solution deployed by Debian and hasn't yet made the required changes. The Debian technical committee's help has been requested multiple times already. Their last decision can be found here: <ulink type=\"block\" url=\"https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=994388#110\" />"
msgstr "Aquest canvi important no està exempt de crear incidències: <command>dpkg</command> haurà d'aprendre sobre aquests directoris enllaçats, però al mantenidor de <command>dpkg</command> no li agrada la solució tècnica desplegada per Debian i encara no ha fet els canvis necessaris. L'ajuda del comitè tècnic de Debian ja ha estat sol·licitada diverses vegades. La seva última decisió es pot trobar aquí: <ulink type=\"block\" url=\"https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=994388#110\" />"

msgid "<primary><command>apt-key</command></primary>"
msgstr "<primary><command>apt-key</command></primary>"

msgid "<command>apt-key</command> will become obsolete. The key management for third party repositories should only rely on keys found in <filename>/etc/apt/trusted.gpg.d</filename> or configured via <literal>Signed-By</literal> as described in <xref linkend=\"sect.package-authentication\" />."
msgstr "<command>apt-key</command> esdevindrà obsolet. La gestió de claus per a repositoris de tercers només ha de dependre de les claus trobades a <filename>/etc/apt/trusted.gpg.d</filename> o configurades amb <literal>Signed-By</literal> com es descriu a <xref linkend=\"sect.package-authentication\" />."

msgid "<primary><command>locate</command></primary>"
msgstr "<primary><command>locate</command></primary>"

msgid "For some tasks the default software solution will change. As an example: <command>plocate</command> might be a faster and smaller replacement for <command>mlocate</command>. <emphasis>systemd</emphasis> will continue to add new features that help to standardize the boot process and system management, allowing us to get rid of some other software in the base system."
msgstr "Per a algunes tasques, la solució de programari predeterminada canviarà. Com a exemple: <command>plocate</command> podria ser un reemplaçament més ràpid i més petit de <command>mlocate</command>. <emphasis>systemd</emphasis> continuarà afegint noves característiques que ajuden a estandarditzar el procés d'arrencada i la gestió del sistema, permetent-nos desfer-nos d'algun altre programari del sistema base."

msgid "Of course all the main software suites will have a major release. The latest version of the various desktops will bring better usability and new features."
msgstr "Per descomptat, tots els principals paquets de programari tindran un canvi de versió important. L'última versió dels diversos escriptoris aportarà una millor usabilitat i noves característiques."

msgid "The default permission of home directories will be more restrictive, allowing only the user to access their files."
msgstr "El permís predeterminat dels directoris d'inici serà més restrictiu, permetent només a l'usuari accedir als seus fitxers."

msgid "Developments which already began will continue: Improve build reproducibility and security for example. With the widespread use of continuous integration and the growth of the archive (and of the biggest packages!), the constraints on release architectures will be harder to meet and architectures will be dropped."
msgstr "Els desenvolupaments que ja han començat continuaran: per exemple, millorar la “reproduïbilitat” de les compilacions i la seguretat. Amb l'ús generalitzat de la integració contínua i el creixement de l'arxiu (i dels paquets més grans!), les restriccions per les arquitectures publicades seran més difícils de complir i n'hi haurà que seran descartades."

msgid "Debian's Future"
msgstr "El futur de Debian"

msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep simplifying this task. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgstr "A part d'aquests desenvolupaments interns, es pot esperar raonablement que surtin a la llum noves distribucions basades en Debian, ja que moltes eines continuen simplificant aquesta tasca. També s'iniciaran nous subprojectes especialitzats, per tal d'ampliar l'abast de Debian cap a nous horitzons."

msgid "The Debian user community will increase, and new contributors will join the project… including, maybe, you!"
msgstr "La comunitat d'usuaris de Debian augmentarà i nous col·laboradors s'uniran al projecte... incloent-vos, potser, a vosaltres!"

msgid "There are recurring discussions about how the software ecosystem is evolving, towards applications shipped within containers, where Debian packages have no added value, or with language-specific package managers (e.g. <command>pip</command> for Python, <command>npm</command> for JavaScript, etc.), which are rendering <command>dpkg</command> and <command>apt</command> obsolete. Facing those threats, I am convinced that Debian developers will find ways to embrace those changes and to continue to provide value to users."
msgstr "Hi ha debats recurrents sobre com evoluciona l'ecosistema de programari, cap a les aplicacions distribuïdes dins de contenidors, on els paquets Debian no tenen cap valor afegit, o amb gestors de paquets específics del llenguatge (p. ex. <command>pip</command> per a Python, <command>npm</command> per a JavaScript, etc.), que estan tornant obsolets <command>dpkg</command> i <command>apt</command>. Davant aquestes amenaces, estic convençut que els desenvolupadors de Debian trobaran la manera d'abastar aquests canvis i de continuar donant valor als usuaris."

msgid "In spite of its old age and its respectable size, Debian keeps on growing in all kinds of (sometimes unexpected) directions. Contributors are teeming with ideas, and discussions on development mailing lists, even when they look like bickerings, keep increasing the momentum. Debian is sometimes compared to a black hole, of such density that any new free software project is attracted."
msgstr "Malgrat l'antiguitat i la mida respectable, Debian continua creixent en tota mena de direccions (de vegades inesperades). Els col·laboradors estan plens d'idees, i les discussions a les llistes de correu de desenvolupament, fins i tot quan sembla que pugen de to, continuen augmentant el ritme. Debian es compara de vegades amb un forat negre, de tal densitat que qualsevol nou projecte de programari lliure és atret."

msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people (and companies!) are increasingly realizing that collaborating, rather than working alone in their corner, leads to better results for everyone. The number of commercial companies relying on Debian is larger every year."
msgstr "Més enllà de l'aparent satisfacció de la majoria dels usuaris de Debian, una tendència profunda és cada vegada més indiscutible: la gent (i les empreses!) s'adona cada vegada més que col·laborar, en lloc de treballar sol en un racó, condueix a millors resultats per a tots. El nombre d'empreses que depenen de Debian és més gran cada any."

msgid "The Debian project is therefore not threatened by extinction…"
msgstr "Per tant, el projecte Debian no està amenaçat d'extinció…"

msgid "Future of this Book"
msgstr "El futur d'aquest llibre"

msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"https://debian-handbook.info/\" /> <ulink type=\"block\" url=\"https://bugs.debian.org/src:debian-handbook\" />"
msgstr "Ens agradaria que aquest llibre evolucionés amb l'esperit del programari lliure. Per tant, acollim amb satisfacció les contribucions, els comentaris, els suggeriments i les crítiques. Si us plau, dirigiu-les al Raphaël (<email>hertzog@debian.org</email>) o al Roland (<email>lolando@debian.org</email>). Per a una resposta activa, no dubteu a obrir informes d'error contra el paquet Debian <literal>debian-handbook</literal>. El lloc web s'utilitzarà per recopilar tota la informació rellevant de la seva evolució, i hi trobareu informació sobre com contribuir, en particular si voleu traduir aquest llibre per a fer-lo accessible a un públic encara més extens que avui. <ulink type=\"block\" url=\"https://debian-handbook.info/\" /> <ulink type=\"block\" url=\"https://bugs.debian.org/src:debian-handbook\" />"

msgid "We tried to integrate most of what our experience with Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgstr "Intentem integrar la major part del que ens va ensenyar la nostra experiència amb Debian, de manera que qualsevol pugui utilitzar aquesta distribució i aprofitar-la al més aviat possible. Esperem que aquest llibre contribueixi a fer que Debian sigui menys confús i més popular, i acollim amb satisfacció la publicitat que se'n fa!"

msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"https://www.freexian.com\" /> <ulink type=\"block\" url=\"https://www.gnurandal.com\" />"
msgstr "Volem acabar amb una nota personal. Escriure (i traduir) aquest llibre ha pres una quantitat considerable de temps a la nostra activitat professional habitual. Com que tots dos som consultors autònoms, qualsevol nova font d'ingressos ens concedeix la llibertat de passar més temps millorant Debian; esperem que aquest llibre tingui èxit i contribueixi a això. Mentrestant, sabeu que podeu contractar els nostres serveis! <ulink type=\"block\" url=\"https://www.freexian.com\" /> <ulink type=\"block\" url=\"https://www.gnurandal.com\" />"

msgid "See you soon!"
msgstr "Fins aviat!"

#~ msgid "In order to improve security and trust, an increasing number of packages will be made to build reproducibly; that is to say, it will be possible to rebuild byte-for-byte identical binary packages from the source packages, thus allowing everyone to verify that no tampering has happened during the builds. This feature might even be required by the release managers for testing migration."
#~ msgstr "Per tal de millorar la seguretat i la confiança, es farà que un nombre creixent de paquets es creïn de manera reproduïble; és a dir, serà possible reconstruir paquets binaris idèntics “byte a byte” a partir dels paquets d'origen, permetent així a tothom verificar que no s'ha produït cap manipulació durant la generació. Aquesta característica pot ser fins i tot requerida pels gestors de publicació per provar la migració."

#~ msgid "In a related theme, a lot of effort will have gone into improving security by default, with more packages shipping an AppArmor profile."
#~ msgstr "Com a tema relacionat, s'haurà fet un gran esforç per millorar la seguretat per defecte, amb més paquets que incloguin un perfil AppArmor."
